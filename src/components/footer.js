// components/Footer.js
import footerData from "../data/footer.json";
import "../styles/Footer.css";

const Footer = () => {
  const { companies, pages, fontstyle } = footerData;
  const currentYear = new Date().getFullYear();


  return (
    <footer className="footer">
        <span className="companies-container">
          {companies.map((company, index) => (
            <span key={index} className="company">
              &copy; {currentYear} {company}
              {index < companies.length - 1 && <span className="separator"> | </span>}
            </span>
          ))}
        </span>
        <span>
        {pages.map((page) => 
          <a href={page.link}>{" | " + page.label}</a> 
        )}
        </span>
    </footer>

  );
};

export default Footer;


